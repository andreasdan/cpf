-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 06, 2020 at 04:57 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbflutter`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(4, '2018_08_01_000000_create_users_table', 1),
(5, '2018_08_02_000000_create_password_resets_table', 1),
(6, '2018_08_03_000000_create_todos_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `todos`
--

CREATE TABLE `todos` (
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('open','closed') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `todos`
--

INSERT INTO `todos` (`created_at`, `updated_at`, `id`, `user_id`, `value`, `status`) VALUES
('2020-09-22 13:00:50', '2020-09-22 13:00:50', 1, 1, 'Amet laudantium rem iste omnis.', 'closed'),
('2020-09-21 13:00:50', '2020-09-21 13:00:50', 2, 1, 'Saepe corrupti et suscipit et qui fuga et qui.', 'closed'),
('2020-09-20 13:00:50', '2020-09-20 13:00:50', 3, 1, 'Fuga dolorem nisi sequi sit rerum qui quod.', 'closed'),
('2020-09-19 13:00:50', '2020-09-19 13:00:50', 4, 1, 'Perferendis est veniam tempora repellendus.', 'open'),
('2020-09-18 13:00:50', '2020-09-18 13:00:50', 5, 1, 'Molestias corrupti quia reiciendis.', 'open'),
('2020-09-17 13:00:50', '2020-09-17 13:00:50', 6, 1, 'Aliquam error quam aut velit dolores.', 'closed'),
('2020-09-16 13:00:50', '2020-09-16 13:00:50', 7, 1, 'Eligendi et dignissimos iure animi assumenda pariatur.', 'closed'),
('2020-09-15 13:00:50', '2020-09-15 13:00:50', 8, 1, 'Error est inventore voluptas harum consequatur dolores explicabo.', 'open'),
('2020-09-14 13:00:50', '2020-09-14 13:00:50', 9, 1, 'Molestiae sit optio est nam.', 'open'),
('2020-09-13 13:00:50', '2020-09-13 13:00:50', 10, 1, 'Quia animi eligendi cupiditate beatae ipsum quia.', 'open'),
('2020-09-12 13:00:50', '2020-09-12 13:00:50', 11, 1, 'Est enim est voluptatem.', 'closed'),
('2020-09-11 13:00:50', '2020-09-11 13:00:50', 12, 1, 'Aut fugit omnis sit et est delectus rem.', 'open'),
('2020-09-10 13:00:50', '2020-09-10 13:00:50', 13, 1, 'Et error aut laborum sunt sapiente cumque ut.', 'open'),
('2020-09-09 13:00:50', '2020-09-09 13:00:50', 14, 1, 'Maiores occaecati illo error sapiente aut magnam libero perferendis.', 'open'),
('2020-09-08 13:00:50', '2020-09-08 13:00:50', 15, 1, 'At a veniam perspiciatis rerum velit nulla enim.', 'open'),
('2020-09-07 13:00:50', '2020-09-07 13:00:50', 16, 1, 'Consequatur totam harum sequi modi ut dolor.', 'closed'),
('2020-09-06 13:00:50', '2020-09-06 13:00:50', 17, 1, 'Vero est id voluptates provident voluptates.', 'closed'),
('2020-09-05 13:00:50', '2020-09-05 13:00:50', 18, 1, 'Est officia impedit minima praesentium delectus aut.', 'closed'),
('2020-09-04 13:00:50', '2020-09-04 13:00:50', 19, 1, 'Reprehenderit reiciendis repellendus recusandae libero qui sit beatae sed.', 'open'),
('2020-09-03 13:00:50', '2020-09-03 13:00:50', 20, 1, 'Commodi quos autem a dolor voluptas culpa.', 'closed'),
('2020-09-02 13:00:50', '2020-09-02 13:00:50', 21, 1, 'Consequuntur hic quidem amet eum unde corporis quis.', 'open'),
('2020-09-01 13:00:50', '2020-09-01 13:00:50', 22, 1, 'Architecto esse a autem non quae dolor dolorum.', 'open'),
('2020-08-31 13:00:50', '2020-08-31 13:00:50', 23, 1, 'Et odit porro molestias eveniet aut aliquam molestias.', 'open'),
('2020-08-30 13:00:50', '2020-08-30 13:00:50', 24, 1, 'Quia vel voluptates repellendus et mollitia ut magnam.', 'closed'),
('2020-08-29 13:00:50', '2020-08-29 13:00:50', 25, 1, 'Perspiciatis ea quas quae rerum quasi veritatis.', 'closed'),
('2020-08-28 13:00:50', '2020-08-28 13:00:50', 26, 1, 'A ut ipsum occaecati illum repudiandae ut enim aliquid.', 'open'),
('2020-08-27 13:00:50', '2020-08-27 13:00:50', 27, 1, 'Consequatur et doloribus soluta non vel voluptates officiis eum.', 'closed'),
('2020-08-26 13:00:50', '2020-09-22 13:05:30', 28, 1, 'Dolores dolorem voluptates esse eum repudiandae.', 'closed'),
('2020-08-25 13:00:50', '2020-08-25 13:00:50', 29, 1, 'Rem tenetur odio ipsam.', 'open'),
('2020-08-24 13:00:50', '2020-08-24 13:00:50', 30, 1, 'Eos aut cumque id et aut repellendus.', 'open'),
('2020-08-23 13:00:50', '2020-08-23 13:00:50', 31, 1, 'Rerum aut omnis iusto.', 'open'),
('2020-09-22 13:00:50', '2020-09-22 13:00:50', 32, 2, 'Ab ea tenetur maiores omnis sed.', 'open'),
('2020-09-21 13:00:50', '2020-09-21 13:00:50', 33, 2, 'Tempora eaque perferendis quae dolorem minima ut.', 'closed'),
('2020-09-20 13:00:50', '2020-09-20 13:00:50', 34, 2, 'Corrupti libero dolorem a soluta.', 'open'),
('2020-09-19 13:00:50', '2020-09-19 13:00:50', 35, 2, 'Et recusandae nesciunt eaque.', 'open'),
('2020-09-18 13:00:50', '2020-09-18 13:00:50', 36, 2, 'Aut nemo eius sit tenetur provident ut.', 'closed'),
('2020-09-17 13:00:50', '2020-09-17 13:00:50', 37, 2, 'Maiores distinctio totam est ullam illo voluptas eligendi.', 'closed'),
('2020-09-16 13:00:50', '2020-09-16 13:00:50', 38, 2, 'Fuga excepturi dolor quia reiciendis quibusdam ratione.', 'open'),
('2020-09-15 13:00:50', '2020-09-15 13:00:50', 39, 2, 'Nulla voluptas reprehenderit cupiditate est voluptates architecto.', 'closed'),
('2020-09-14 13:00:50', '2020-09-14 13:00:50', 40, 2, 'Quo vel quam nesciunt amet facilis expedita.', 'open'),
('2020-09-13 13:00:50', '2020-09-13 13:00:50', 41, 2, 'Pariatur ut natus inventore nostrum nostrum.', 'open'),
('2020-09-12 13:00:50', '2020-09-12 13:00:50', 42, 2, 'Vero eos fugiat modi mollitia doloribus porro deserunt.', 'closed'),
('2020-09-11 13:00:50', '2020-09-11 13:00:50', 43, 2, 'Iusto qui aut non illum rem enim aut.', 'open'),
('2020-09-10 13:00:50', '2020-09-10 13:00:50', 44, 2, 'Et ullam nesciunt voluptas.', 'closed'),
('2020-09-09 13:00:50', '2020-09-09 13:00:50', 45, 2, 'Neque numquam impedit consequatur voluptatem.', 'closed'),
('2020-09-08 13:00:50', '2020-09-08 13:00:50', 46, 2, 'Voluptatibus et facere quod.', 'closed'),
('2020-09-07 13:00:50', '2020-09-07 13:00:50', 47, 2, 'Eius incidunt nisi repudiandae nemo provident neque aut.', 'closed'),
('2020-09-06 13:00:50', '2020-09-06 13:00:50', 48, 2, 'Consequatur sequi illo vitae molestiae aliquam sed.', 'closed'),
('2020-09-05 13:00:50', '2020-09-05 13:00:50', 49, 2, 'Eveniet et similique labore consequatur.', 'open'),
('2020-09-04 13:00:50', '2020-09-04 13:00:50', 50, 2, 'Corrupti corrupti cum quo laborum perspiciatis sint sed nesciunt.', 'open'),
('2020-09-03 13:00:50', '2020-09-03 13:00:50', 51, 2, 'Consequatur deleniti a est et et amet dolor.', 'closed'),
('2020-09-02 13:00:50', '2020-09-02 13:00:50', 52, 2, 'Ipsum animi voluptatibus nulla qui vel ex.', 'closed'),
('2020-09-01 13:00:50', '2020-09-01 13:00:50', 53, 2, 'Dolores illo rerum sunt sit.', 'closed'),
('2020-08-31 13:00:50', '2020-08-31 13:00:50', 54, 2, 'Magni dolor ea adipisci qui.', 'open'),
('2020-08-30 13:00:50', '2020-08-30 13:00:50', 55, 2, 'Labore provident aut voluptatem omnis dolor aliquam dolores.', 'open'),
('2020-08-29 13:00:50', '2020-08-29 13:00:50', 56, 2, 'Quibusdam impedit dolor et ut enim.', 'closed'),
('2020-08-28 13:00:50', '2020-08-28 13:00:50', 57, 2, 'Dolore quia non sit similique tempora non eius.', 'open'),
('2020-08-27 13:00:50', '2020-08-27 13:00:50', 58, 2, 'Pariatur suscipit pariatur reprehenderit aliquid.', 'closed'),
('2020-08-26 13:00:50', '2020-08-26 13:00:50', 59, 2, 'Modi quaerat quae et debitis aperiam libero.', 'open'),
('2020-08-25 13:00:50', '2020-08-25 13:00:50', 60, 2, 'Ex est id exercitationem ipsum cupiditate.', 'open'),
('2020-08-24 13:00:50', '2020-08-24 13:00:50', 61, 2, 'Blanditiis consequuntur deserunt et nisi culpa.', 'open'),
('2020-08-23 13:00:50', '2020-08-23 13:00:50', 62, 2, 'Molestiae pariatur at magni repudiandae quidem.', 'open'),
('2020-09-22 13:00:50', '2020-09-22 13:00:50', 63, 3, 'Ipsam corrupti tenetur numquam.', 'closed'),
('2020-09-21 13:00:50', '2020-09-21 13:00:50', 64, 3, 'Voluptas temporibus voluptatem velit odit commodi et est.', 'closed'),
('2020-09-20 13:00:50', '2020-09-20 13:00:50', 65, 3, 'Qui sit sint incidunt aliquam dolorem.', 'open'),
('2020-09-19 13:00:50', '2020-09-19 13:00:50', 66, 3, 'Quisquam est non et magnam sequi explicabo.', 'closed'),
('2020-09-18 13:00:50', '2020-09-18 13:00:50', 67, 3, 'Voluptatum quia ducimus ullam consequatur perferendis quidem quasi.', 'open'),
('2020-09-17 13:00:50', '2020-09-17 13:00:50', 68, 3, 'Eaque labore debitis molestiae.', 'open'),
('2020-09-16 13:00:50', '2020-09-16 13:00:50', 69, 3, 'Sed sint et voluptas.', 'open'),
('2020-09-15 13:00:50', '2020-09-15 13:00:50', 70, 3, 'Molestiae neque enim at et.', 'open'),
('2020-09-14 13:00:50', '2020-09-14 13:00:50', 71, 3, 'Et modi quia commodi fuga recusandae.', 'open'),
('2020-09-13 13:00:50', '2020-09-13 13:00:50', 72, 3, 'Voluptas expedita eligendi dolorum non dolorem delectus dignissimos.', 'closed'),
('2020-09-12 13:00:50', '2020-09-12 13:00:50', 73, 3, 'Quis cupiditate facilis maxime dolor est aliquam possimus.', 'closed'),
('2020-09-11 13:00:50', '2020-09-11 13:00:50', 74, 3, 'Vitae quasi et earum corporis.', 'open'),
('2020-09-10 13:00:50', '2020-09-10 13:00:50', 75, 3, 'Molestias deleniti ut et sed aut exercitationem et.', 'open'),
('2020-09-09 13:00:50', '2020-09-09 13:00:50', 76, 3, 'Velit quaerat aspernatur enim ipsum eius.', 'open'),
('2020-09-08 13:00:50', '2020-09-08 13:00:50', 77, 3, 'Magni mollitia modi sed autem vel iusto deleniti qui.', 'closed'),
('2020-09-07 13:00:50', '2020-09-07 13:00:50', 78, 3, 'Quisquam voluptas pariatur qui culpa corporis non vel.', 'closed'),
('2020-09-06 13:00:50', '2020-09-06 13:00:50', 79, 3, 'Quaerat quo vitae ut nemo itaque.', 'closed'),
('2020-09-05 13:00:50', '2020-09-05 13:00:50', 80, 3, 'Esse a autem est praesentium et.', 'open'),
('2020-09-04 13:00:50', '2020-09-04 13:00:50', 81, 3, 'Aperiam dolorum ipsa expedita non sunt blanditiis.', 'open'),
('2020-09-03 13:00:50', '2020-09-03 13:00:50', 82, 3, 'Amet pariatur quam quia sunt temporibus.', 'open'),
('2020-09-02 13:00:50', '2020-09-02 13:00:50', 83, 3, 'Est dolor ea nemo qui.', 'open'),
('2020-09-01 13:00:50', '2020-09-01 13:00:50', 84, 3, 'Incidunt vel tempore consequatur eligendi.', 'open'),
('2020-08-31 13:00:50', '2020-08-31 13:00:50', 85, 3, 'Repellat laborum sed neque nihil.', 'open'),
('2020-08-30 13:00:50', '2020-08-30 13:00:50', 86, 3, 'Ut dolor illo eius doloribus.', 'closed'),
('2020-08-29 13:00:50', '2020-08-29 13:00:50', 87, 3, 'Magnam perspiciatis dicta et velit maiores possimus quo.', 'open'),
('2020-08-28 13:00:50', '2020-08-28 13:00:50', 88, 3, 'Reprehenderit voluptas voluptatem nulla doloremque.', 'closed'),
('2020-08-27 13:00:50', '2020-08-27 13:00:50', 89, 3, 'Magni sed autem fugiat eos enim aut.', 'open'),
('2020-08-26 13:00:50', '2020-08-26 13:00:50', 90, 3, 'Nemo animi maiores optio et qui hic.', 'closed'),
('2020-08-25 13:00:50', '2020-08-25 13:00:50', 91, 3, 'Qui autem minima sint omnis tenetur perspiciatis.', 'open'),
('2020-08-24 13:00:50', '2020-08-24 13:00:50', 92, 3, 'Nobis reprehenderit ad quis deleniti unde facere perspiciatis.', 'closed'),
('2020-08-23 13:00:50', '2020-08-23 13:00:50', 93, 3, 'Omnis non qui sint sit hic atque voluptatem.', 'closed'),
('2020-09-22 13:00:50', '2020-09-22 13:00:50', 94, 4, 'Nam itaque earum ad adipisci.', 'closed'),
('2020-09-21 13:00:50', '2020-09-21 13:00:50', 95, 4, 'Eveniet a sit inventore quo recusandae in non.', 'closed'),
('2020-09-20 13:00:50', '2020-09-20 13:00:50', 96, 4, 'Accusantium et aut officia voluptas.', 'open'),
('2020-09-19 13:00:50', '2020-09-19 13:00:50', 97, 4, 'Ratione minus dolores molestiae placeat.', 'open'),
('2020-09-18 13:00:50', '2020-09-18 13:00:50', 98, 4, 'Aut nihil recusandae quam voluptatem laboriosam.', 'closed'),
('2020-09-17 13:00:50', '2020-09-17 13:00:50', 99, 4, 'Et quisquam iusto aliquam velit incidunt atque nemo.', 'closed'),
('2020-09-16 13:00:50', '2020-09-16 13:00:50', 100, 4, 'Dignissimos possimus asperiores ipsum placeat veniam id quia.', 'closed'),
('2020-09-15 13:00:50', '2020-09-15 13:00:50', 101, 4, 'Incidunt alias delectus animi voluptatem omnis repellendus dolorem.', 'open'),
('2020-09-14 13:00:50', '2020-09-14 13:00:50', 102, 4, 'Deserunt officiis eius magni quia alias corporis alias.', 'open'),
('2020-09-13 13:00:50', '2020-09-13 13:00:50', 103, 4, 'Hic ab unde quia quia deserunt.', 'open'),
('2020-09-12 13:00:50', '2020-09-12 13:00:50', 104, 4, 'Consequuntur et maiores sapiente et.', 'closed'),
('2020-09-11 13:00:50', '2020-09-11 13:00:50', 105, 4, 'Nesciunt voluptates vel natus sunt aliquam.', 'closed'),
('2020-09-10 13:00:50', '2020-09-10 13:00:50', 106, 4, 'Vel aspernatur esse nisi vel ut velit.', 'closed'),
('2020-09-09 13:00:50', '2020-09-09 13:00:50', 107, 4, 'Voluptatem quam aut qui aut quidem doloremque.', 'closed'),
('2020-09-08 13:00:50', '2020-09-08 13:00:50', 108, 4, 'Qui et et ut consequatur aut sit quidem.', 'closed'),
('2020-09-07 13:00:50', '2020-09-07 13:00:50', 109, 4, 'Earum quis aliquam sunt consequatur molestiae laboriosam aspernatur.', 'open'),
('2020-09-06 13:00:50', '2020-09-06 13:00:50', 110, 4, 'Aut cum quae est voluptates.', 'closed'),
('2020-09-05 13:00:50', '2020-09-05 13:00:50', 111, 4, 'Accusantium voluptatum incidunt sed atque et consequatur.', 'closed'),
('2020-09-04 13:00:50', '2020-09-04 13:00:50', 112, 4, 'Illum error in et qui.', 'open'),
('2020-09-03 13:00:50', '2020-09-03 13:00:50', 113, 4, 'Consequatur ipsa maxime occaecati cupiditate.', 'closed'),
('2020-09-02 13:00:50', '2020-09-02 13:00:50', 114, 4, 'Quia consequatur qui rerum.', 'closed'),
('2020-09-01 13:00:50', '2020-09-01 13:00:50', 115, 4, 'Sequi reiciendis dolorem distinctio delectus sit aspernatur.', 'closed'),
('2020-08-31 13:00:50', '2020-08-31 13:00:50', 116, 4, 'Aut consequatur similique ipsam minima iusto.', 'closed'),
('2020-08-30 13:00:50', '2020-08-30 13:00:50', 117, 4, 'Totam esse aut quisquam non dolor a.', 'closed'),
('2020-08-29 13:00:50', '2020-08-29 13:00:50', 118, 4, 'Qui harum sed assumenda recusandae deleniti quae ut.', 'closed'),
('2020-08-28 13:00:50', '2020-08-28 13:00:50', 119, 4, 'Est officia sit ipsum rerum beatae et.', 'open'),
('2020-08-27 13:00:50', '2020-08-27 13:00:50', 120, 4, 'Consequatur vero aspernatur ut incidunt fugit corrupti quia labore.', 'open'),
('2020-08-26 13:00:50', '2020-08-26 13:00:50', 121, 4, 'Omnis accusantium quasi labore nesciunt quia tenetur ut.', 'open'),
('2020-08-25 13:00:50', '2020-08-25 13:00:50', 122, 4, 'Blanditiis illo similique eum quisquam est est provident.', 'closed'),
('2020-08-24 13:00:50', '2020-08-24 13:00:50', 123, 4, 'Qui ab non rem incidunt.', 'closed'),
('2020-08-23 13:00:50', '2020-08-23 13:00:50', 124, 4, 'Occaecati ex asperiores voluptatibus doloremque.', 'closed'),
('2020-09-22 13:00:50', '2020-09-22 13:00:50', 125, 5, 'Quod ut aspernatur dolores blanditiis cum numquam facere qui.', 'closed'),
('2020-09-21 13:00:50', '2020-09-21 13:00:50', 126, 5, 'Aspernatur distinctio commodi ipsa quaerat qui et corrupti.', 'open'),
('2020-09-20 13:00:50', '2020-09-20 13:00:50', 127, 5, 'Ut ut est tempora.', 'open'),
('2020-09-19 13:00:50', '2020-09-19 13:00:50', 128, 5, 'Quas occaecati placeat odit quasi molestias.', 'closed'),
('2020-09-18 13:00:50', '2020-09-18 13:00:50', 129, 5, 'Facere eum ipsum sed soluta autem.', 'closed'),
('2020-09-17 13:00:50', '2020-09-17 13:00:50', 130, 5, 'Omnis omnis qui velit alias quisquam dolor voluptate omnis.', 'open'),
('2020-09-16 13:00:50', '2020-09-16 13:00:50', 131, 5, 'Nihil illo ut sed expedita iure est.', 'open'),
('2020-09-15 13:00:50', '2020-09-15 13:00:50', 132, 5, 'Dolores dolor rerum ipsum neque unde sunt inventore.', 'closed'),
('2020-09-14 13:00:50', '2020-09-14 13:00:50', 133, 5, 'Amet qui dolor fuga accusamus consequatur error.', 'closed'),
('2020-09-13 13:00:50', '2020-09-13 13:00:50', 134, 5, 'Aut hic aut qui accusantium qui officiis.', 'closed'),
('2020-09-12 13:00:50', '2020-09-12 13:00:50', 135, 5, 'Aut dolores sit neque adipisci quisquam quam.', 'closed'),
('2020-09-11 13:00:50', '2020-09-11 13:00:50', 136, 5, 'Minus sunt qui corrupti illo.', 'closed'),
('2020-09-10 13:00:50', '2020-09-10 13:00:50', 137, 5, 'Veritatis eius asperiores aut est sed aperiam.', 'open'),
('2020-09-09 13:00:50', '2020-09-09 13:00:50', 138, 5, 'Laborum esse alias consequatur optio animi soluta est.', 'closed'),
('2020-09-08 13:00:50', '2020-09-08 13:00:50', 139, 5, 'Voluptatem neque sed enim tempora qui perspiciatis.', 'open'),
('2020-09-07 13:00:50', '2020-09-07 13:00:50', 140, 5, 'Autem omnis eius eos sint sint assumenda.', 'open'),
('2020-09-06 13:00:50', '2020-09-06 13:00:50', 141, 5, 'Impedit harum nisi libero similique debitis explicabo voluptate.', 'closed'),
('2020-09-05 13:00:50', '2020-09-05 13:00:50', 142, 5, 'Recusandae occaecati enim deleniti laborum sequi.', 'closed'),
('2020-09-04 13:00:50', '2020-09-04 13:00:50', 143, 5, 'Porro ipsum sed officia eum itaque ipsum.', 'closed'),
('2020-09-03 13:00:50', '2020-09-03 13:00:50', 144, 5, 'Ullam animi illo enim ut reiciendis officia.', 'open'),
('2020-09-02 13:00:50', '2020-09-02 13:00:50', 145, 5, 'Fuga impedit aut et ducimus est neque id repellendus.', 'closed'),
('2020-09-01 13:00:50', '2020-09-01 13:00:50', 146, 5, 'Reprehenderit ab odit et sit minima.', 'open'),
('2020-08-31 13:00:50', '2020-08-31 13:00:50', 147, 5, 'Suscipit aut numquam voluptatem est.', 'open'),
('2020-08-30 13:00:50', '2020-08-30 13:00:50', 148, 5, 'Maiores accusamus quisquam sint quaerat est repellat.', 'open'),
('2020-08-29 13:00:50', '2020-08-29 13:00:50', 149, 5, 'Consequatur enim omnis velit dolorem voluptatem corporis sed.', 'open'),
('2020-08-28 13:00:50', '2020-08-28 13:00:50', 150, 5, 'Qui voluptates quia nostrum similique aut in sapiente voluptatem.', 'closed'),
('2020-08-27 13:00:50', '2020-08-27 13:00:50', 151, 5, 'Quibusdam amet corporis perspiciatis sunt eaque hic error.', 'closed'),
('2020-08-26 13:00:50', '2020-08-26 13:00:50', 152, 5, 'Nemo voluptas quo sapiente aliquam ipsum voluptas vero.', 'open'),
('2020-08-25 13:00:50', '2020-08-25 13:00:50', 153, 5, 'Est hic ipsam eveniet ex nostrum temporibus.', 'closed'),
('2020-08-24 13:00:50', '2020-08-24 13:00:50', 154, 5, 'Voluptas laudantium qui consequuntur labore dolorem ab.', 'open'),
('2020-08-23 13:00:50', '2020-08-23 13:00:50', 155, 5, 'Voluptate architecto tempore debitis quod perspiciatis cumque et.', 'open'),
('2020-09-22 13:00:50', '2020-09-22 13:00:50', 156, 6, 'Distinctio ullam qui delectus commodi totam fuga.', 'closed'),
('2020-09-21 13:00:50', '2020-09-21 13:00:50', 157, 6, 'Suscipit placeat voluptate quia.', 'closed'),
('2020-09-20 13:00:50', '2020-09-20 13:00:50', 158, 6, 'Enim et qui similique sunt et pariatur.', 'open'),
('2020-09-19 13:00:50', '2020-09-19 13:00:50', 159, 6, 'Sed ea dolor deleniti minus.', 'closed'),
('2020-09-18 13:00:50', '2020-09-18 13:00:50', 160, 6, 'Et debitis aut quo consectetur cumque maiores.', 'closed'),
('2020-09-17 13:00:50', '2020-09-17 13:00:50', 161, 6, 'Sed omnis dolorum iure.', 'open'),
('2020-09-16 13:00:50', '2020-09-16 13:00:50', 162, 6, 'Sed earum ea omnis omnis.', 'closed'),
('2020-09-15 13:00:50', '2020-09-15 13:00:50', 163, 6, 'Est laboriosam ea atque ea nihil sapiente.', 'open'),
('2020-09-14 13:00:50', '2020-09-14 13:00:50', 164, 6, 'Sed et distinctio facilis dolores debitis aut.', 'open'),
('2020-09-13 13:00:50', '2020-09-13 13:00:50', 165, 6, 'Nisi voluptatem rerum consequatur corporis qui quam ab.', 'open'),
('2020-09-12 13:00:50', '2020-09-12 13:00:50', 166, 6, 'Odio aut exercitationem eius aliquid.', 'open'),
('2020-09-11 13:00:50', '2020-09-11 13:00:50', 167, 6, 'Minima sint atque neque fuga iusto.', 'open'),
('2020-09-10 13:00:50', '2020-09-10 13:00:50', 168, 6, 'Quidem dicta dicta est iure voluptas quas atque autem.', 'closed'),
('2020-09-09 13:00:50', '2020-09-09 13:00:50', 169, 6, 'Et odio fuga placeat dolor.', 'open'),
('2020-09-08 13:00:50', '2020-09-08 13:00:50', 170, 6, 'Quos occaecati odio illo minima dolore sit.', 'open'),
('2020-09-07 13:00:50', '2020-09-07 13:00:50', 171, 6, 'Ipsam quo est maxime illo provident est dolorem.', 'closed'),
('2020-09-06 13:00:50', '2020-09-06 13:00:50', 172, 6, 'Aperiam exercitationem cum repellat aut.', 'open'),
('2020-09-05 13:00:50', '2020-09-05 13:00:50', 173, 6, 'Quidem hic id id fuga voluptatem.', 'closed'),
('2020-09-04 13:00:50', '2020-09-04 13:00:50', 174, 6, 'Optio laudantium dolore et sed velit consequatur.', 'closed'),
('2020-09-03 13:00:50', '2020-09-03 13:00:50', 175, 6, 'Rerum architecto pariatur quis.', 'closed'),
('2020-09-02 13:00:50', '2020-09-02 13:00:50', 176, 6, 'Perferendis accusantium magnam aspernatur deserunt.', 'open'),
('2020-09-01 13:00:50', '2020-09-01 13:00:50', 177, 6, 'Qui sapiente asperiores et aperiam odit quo asperiores.', 'open'),
('2020-08-31 13:00:50', '2020-08-31 13:00:50', 178, 6, 'Sapiente non est ut libero qui magnam.', 'open'),
('2020-08-30 13:00:50', '2020-08-30 13:00:50', 179, 6, 'Iusto culpa consequatur doloribus officia.', 'open'),
('2020-08-29 13:00:50', '2020-08-29 13:00:50', 180, 6, 'Iusto quam dolorem odio dolorem iste delectus.', 'open'),
('2020-08-28 13:00:50', '2020-08-28 13:00:50', 181, 6, 'Omnis quis dolorum voluptatem quia ipsa officiis necessitatibus.', 'closed'),
('2020-08-27 13:00:50', '2020-08-27 13:00:50', 182, 6, 'Sint nemo dolorem temporibus et eius rerum.', 'open'),
('2020-08-26 13:00:50', '2020-08-26 13:00:50', 183, 6, 'Dolorem sit enim pariatur reprehenderit deserunt ut commodi.', 'closed'),
('2020-08-25 13:00:50', '2020-08-25 13:00:50', 184, 6, 'Tenetur atque numquam quasi explicabo in aliquid quam.', 'closed'),
('2020-08-24 13:00:50', '2020-08-24 13:00:50', 185, 6, 'Aut et ut labore similique.', 'open'),
('2020-08-23 13:00:50', '2020-08-23 13:00:50', 186, 6, 'Dolorem aperiam dolorem dignissimos laboriosam harum voluptas quam.', 'closed'),
('2020-09-22 13:05:42', '2020-09-22 13:07:04', 187, 1, 'Andreas Dan', 'closed'),
('2020-09-22 13:09:33', '2020-09-22 13:09:33', 188, 1, 'DanReact', 'open'),
('2020-09-23 03:32:03', '2020-09-23 14:00:30', 189, 7, 'Yeeee', 'open'),
('2020-09-23 03:32:09', '2020-09-23 03:32:09', 190, 7, 'FDasasa', 'open'),
('2020-09-23 08:52:55', '2020-09-23 08:52:55', 191, 7, 'Oke', 'open'),
('2020-09-23 09:38:57', '2020-09-23 13:41:42', 192, 7, 'XC', 'open'),
('2020-09-23 10:19:17', '2020-09-23 10:19:17', 193, 8, 'gfdhjgv', 'open'),
('2020-09-23 10:19:46', '2020-09-23 10:19:46', 194, 8, 'xxxxxx', 'open'),
('2020-09-23 10:20:41', '2020-09-23 10:22:51', 195, 8, 'Rifky', 'closed'),
('2020-09-23 10:22:59', '2020-09-23 10:22:59', 196, 8, 'Fajri', 'open');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`created_at`, `updated_at`, `id`, `name`, `email`, `password`, `remember_token`) VALUES
('2020-09-22 13:00:50', '2020-09-22 13:00:50', 1, 'User Test', 'user@test.dev', '$2y$10$LOvNfGhel/BAJme/XQMcXeRDEnokbnMJKhKWM0DnOAIt9Op0SWApG', NULL),
('2020-09-22 13:00:50', '2020-09-22 13:00:50', 2, 'Brendan Stamm', 'wbrekke@example.org', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '72P8P6A3bl'),
('2020-09-22 13:00:50', '2020-09-22 13:00:50', 3, 'Ebba Prosacco V', 'spencer.marilyne@example.com', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'x2WVwUYJ00'),
('2020-09-22 13:00:50', '2020-09-22 13:00:50', 4, 'Ashlee Hoeger', 'strosin.vernie@example.net', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'z6i0ynFqa6'),
('2020-09-22 13:00:50', '2020-09-22 13:00:50', 5, 'Kailyn Pfannerstill', 'dorcas22@example.org', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '0GbTukJAnx'),
('2020-09-22 13:00:50', '2020-09-22 13:00:50', 6, 'Alexandrea O\'Keefe', 'schuster.frankie@example.org', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '182YADnVfK'),
('2020-09-23 03:29:48', '2020-09-23 03:29:48', 7, 'Master Ungguh', 'master@gmail.com', '$2y$10$kpwX36nF/GiR25.1t1S.A.1M1Iv97yxxo8JiZ6P5Pk3Kwo6fdM7IO', NULL),
('2020-09-23 08:56:47', '2020-09-23 08:56:47', 8, 'Adnan', 'adnan@gmail.com', '$2y$10$sQVv6XvIstH8/Owqeqbl/uHMpiLgT6ubAaYx0kYkI9yK8bta71XMi', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `todos`
--
ALTER TABLE `todos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `todos`
--
ALTER TABLE `todos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=197;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
